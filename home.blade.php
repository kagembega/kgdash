@extends('layouts.front')

@section('title', ' Le Meilleur Site Ecommerce')
@section('content')
  
        <main>
        <div id="carousel-home">
            <div class="owl-carousel owl-theme">
            @foreach($bannerPrincipal as $banmain)
                <div class="owl-slide cover" style="background-image: url('{{Voyager::image($banmain->images)}}');">
                     {{--<img src="{{ asset('storage/' .$banmain->images) }}" alt="">--}}
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-12 static">
                                    <div class="slide-text text-center white">
                                        <h2 class="owl-slide-animated owl-slide-title">{!!$banmain->titre!!}</h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            {!!$banmain->commentaire!!}
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta"><a class="btn_1" href="listing-grid-1-full.html" role="button">Acheter Maintenant</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            </div>
            <div id="icon_drag_mobile"></div>
        </div>
        <!--/carousel-->
        <ul id="banners_grid" class="clearfix">
        @foreach($bannerCollection as $bancollec)
        <li>
            <a href="#0" class="img_container">
                <img src="{{Voyager::image($bancollec->images)}}" data-src="{{Voyager::image($bancollec->images)}}" alt="" class="lazy">
                <div class="short_info opacity-mask" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                    <h3>Collection {{$bancollec->category->name}}</h3>
                    <div><span class="btn_1">Acheter Maintenant</span></div>
                </div>
            </a>
        </li>
        @endforeach
        </ul>
        <!--/banners_grid -->

            @livewire('main-product')

            <!-- /row -->
       
        <!-- /container -->
    @foreach($bannerAutre as $banautre)
        <div class="featured lazy" data-bg="url('{{Voyager::image($banautre->images)}}')">
            <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                <div class="container margin_60">
                    <div class="row justify-content-center justify-content-md-start">
                        <div class="col-lg-6 wow" data-wow-offset="150">
                            <h3>{!!$banautre->titre!!}</h3>
                            <p>{!!$banautre->commentaire!!}</p>
                            <div class="feat_text_block">
                                <div class="price_box">
                                    <span class="new_price">$90.00</span>
                                    <span class="old_price">$170.00</span>
                                </div>
                                <a class="btn_1" href="listing-grid-1-full.html" role="button">Acheter Maintenant</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /featured -->
    @endforeach

        <div class="container margin_60_35">
            <div class="main_title">
                <h2>En vedette</h2>
                <span>Produits</span>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset</p>
            </div>

            <div class="owl-carousel owl-theme products_carousel">

                @foreach($productsSlide as $product)
                    <div class="item">
                        <div class="grid_item">
                            <span class="ribbon new">Nouveau</span>
                            <figure>
                                <a href="/products/{{$product->slug}}">
                                    <img class="owl-lazy" src="{{Voyager::image($product->cover_img)}}" data-src="{{Voyager::image($product->cover_img)}}" alt="">
                                </a>
                            </figure>
                            <div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i></div>
                            <a href="/products/{{$product->slug}}">
                                <h3>{{$product->name}}</h3>
                            </a>
                            <div class="price_box">
                                <span class="new_price">{{$product->price}} fcfa</span>
                            </div>
                            <ul>
                                <li><a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left" title="Ajouter aux favoris"><i class="ti-heart"></i><span>Ajouter aux favoris</span></a></li>
                                <li><a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left" title="Ajouter pour comparer"><i class="ti-control-shuffle"></i><span>Ajouter pour comparer</span></a></li>
                                
                               @if(sizeof($product->sizeclothings)!=0 || sizeof($product->sizeshoes)!=0 || sizeof($product->colors)!=0)
                                <li><a href="/products/{{$product->slug}}" class="tooltip-1" data-toggle="tooltip" data-placement="left" title="Ajouter au panier"><i class="ti-shopping-cart"></i><span>Ajouter au panier</span></a></li>
                                @else
                                 <li><a href="{{ route('cart.add',$product->slug) }}" class="tooltip-1" data-toggle="tooltip" data-placement="left" title="Ajouter au panier"><i class="ti-shopping-cart"></i><span>Ajouter au panier</span></a></li>
                                @endif
                            </ul>
                        </div>
                        <!-- /grid_item -->
                    </div>
                 @endforeach


                <!-- /item -->
            </div>
            <!-- /products_carousel -->
        </div>
        <!-- /container -->


    @if($partenaires->count() != 0)
        <div class="bg_gray">
            <div class="container margin_30">
                <div id="brands" class="owl-carousel owl-theme">
                    @foreach($partenaires as $partenaire)
                    <div class="item">
                        <a href="#0"><img src="{{Voyager::image($partenaire->images)}}" data-src="{{Voyager::image($partenaire->images)}}" alt="" class="owl-lazy"></a>
                    </div><!-- /item -->
                    @endforeach
                </div><!-- /carousel -->
            </div>
        </div>
    @endif

        <div class="container margin_60_35">
            <div class="main_title">
                <h2>Dernières actualités</h2>
                <span>Blog</span>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset</p>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <a class="box_news" href="blog.html">
                        <figure>
                            <img src="/img/blog-thumb-placeholder.jpg" data-src="/img/blog-thumb-1.jpg" alt="" width="400" height="266" class="lazy">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>by Mark Twain</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Pri oportere scribentur eu</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <!-- /box_news -->
                <div class="col-lg-6">
                    <a class="box_news" href="blog.html">
                        <figure>
                            <img src="/img/blog-thumb-placeholder.jpg" data-src="/img/blog-thumb-2.jpg" alt="" width="400" height="266" class="lazy">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>By Jhon Doe</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Duo eius postea suscipit ad</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <!-- /box_news -->
                <div class="col-lg-6">
                    <a class="box_news" href="blog.html">
                        <figure>
                            <img src="/img/blog-thumb-placeholder.jpg" data-src="/img/blog-thumb-3.jpg" alt="" width="400" height="266" class="lazy">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>By Luca Robinson</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Elitr mandamus cu has</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <!-- /box_news -->
                <div class="col-lg-6">
                    <a class="box_news" href="blog.html">
                        <figure>
                            <img src="/img/blog-thumb-placeholder.jpg" data-src="/img/blog-thumb-4.jpg" alt="" width="400" height="266" class="lazy">
                            <figcaption><strong>28</strong>Dec</figcaption>
                        </figure>
                        <ul>
                            <li>By Paula Rodrigez</li>
                            <li>20.11.2017</li>
                        </ul>
                        <h4>Id est adhuc ignota delenit</h4>
                        <p>Cu eum alia elit, usu in eius appareat, deleniti sapientem honestatis eos ex. In ius esse ullum vidisse....</p>
                    </a>
                </div>
                <!-- /box_news -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </main>

@endsection

